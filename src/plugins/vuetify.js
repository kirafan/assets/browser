// src/plugins/vuetify.js

import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

export default new Vuetify({
  breakpoint: {
    thresholds: {
      xs: 360,
      sm: 720,
      md: 1080,
      lg: 1440,
    },
    scrollBarWidth: 0,
  },
  theme: {
    themes: {
      light: {
        primary: colors.brown.lighten2,
        secondary: colors.brown.lighten4,
        accent: colors.indigo.base,
      },
    },
  },
});