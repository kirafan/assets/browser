import axios from 'axios';
import JSZip from 'jszip';
import fileSaver from 'file-saver';

import assetBundle from '@/assetBundle';

const jobs = [];

class Job {
    constructor(paths, name) {
        this.paths = paths.slice();
        this.name = name;

        this.count = 0;
        this.total = this.paths.length;

        this.zip = new JSZip();
        this.zipProgress = 0;

        this.threads = [];
        this.status = 0; // 0: dl; 1: waiting; 2: zipping; 3: finished

        for (let i = 0; i < 16; i++) {
            this.thread();
        }
    }

    download(path) {
        return axios.get(assetBundle.url(path, { type: 'index' })).then(response => {
            const names = response.data;
            const promises = names.map(name => axios({
                method: 'get',
                url: assetBundle.url(path, { name: name + '.png' }),
                responseType: 'blob',
            }).then(response => {
                this.zip.file(`${path}/${name}.png`, response.data, {
                    binary: true,
                    createFolders: true,
                });
            }));
            return Promise.all(promises);
        }).catch(() => {
            return;
        });
    }

    thread() {
        const path = this.paths.pop();
        if (!path) {
            return this.save();
        }
        const promise = this.download(path);
        this.threads.push(promise);
        promise.then(() => {
            this.count += 1;
            this.thread();
        });
    }

    save() {
        if (this.status == 1) {
            return;
        }
        this.status = 1;
        Promise.all(this.threads).then(() => {
            this.status = 2;
            this.zip.generateAsync({
                type: 'blob',
            }, metadata => {
                this.zipProgress = metadata.percent / 100;
            }).then(content => {
                fileSaver.saveAs(content, `${this.name}.zip`);
                this.status = 3;
            });
        });
    }

    get progress() {
        if (this.status <= 1) {
            return this.count / this.total;
        } else if (this.status == 2) {
            return this.zipProgress;
        } else {
            return 1;
        }
    }
}

window.jobs = jobs;

function createJob(paths, name) {
    jobs.push(new Job(paths, name));
}

export default { createJob, jobs };
