import axios from 'axios';

let assetBundle = {};

function get(path) {
    let paths = path.split('/');
    let folder = assetBundle;
    for (let name of paths) {
        if (name == '') continue;
        if (!folder[name]) folder[name] = {};
        folder = folder[name];
    }
    return folder;
}

function isDir(folder) {
    // console.log('isDir', folder);
    if (typeof folder === typeof '') return isDir(get(folder));
    return folder.size === undefined;
}

function size(size) {
    if (size < 1e3) {
        return `${size.toFixed(0)} B`;
    }
    if (size < 1e4) {
        return `${(size / 1e3).toFixed(2)} KB`;
    }
    if (size < 1e5) {
        return `${(size / 1e3).toFixed(1)} KB`;
    }
    if (size < 1e6) {
        return `${(size / 1e3).toFixed(0)} KB`;
    }
    if (size < 1e7) {
        return `${(size / 1e6).toFixed(2)} MB`;
    }
    if (size < 1e8) {
        return `${(size / 1e6).toFixed(1)} MB`;
    }
    if (size < 1e9) {
        return `${(size / 1e6).toFixed(0)} MB`;
    }
    if (size < 1e10) {
        return `${(size / 1e9).toFixed(2)} GB`;
    }
    if (size < 1e11) {
        return `${(size / 1e9).toFixed(1)} GB`;
    }
    if (size < 1e12) {
        return `${(size / 1e9).toFixed(0)} GB`;
    }
}

axios.get('https://database.kirafan.cn/assetBundle.json').then(response => {
    for (let asset of response.data) {
        let paths = asset.name.split('/');
        let folder = get(paths.slice(0, paths.length - 1).join('/'));
        folder[paths[paths.length - 1]] = asset;
    }
    window.vue.$emit('ok', true);
});

function list(file, recursive) {
    const paths = [];
    if (!isDir(file)) {
        paths.push(file.name);
    } else if (recursive) {
        const files = file;
        for (let name in files) {
            paths.push(...list(files[name], recursive));
        }
    }
    return paths;
}

function url(path, { type, name } = {}) {
    const file = get(path);
    const bucket = file.path[file.path.length - 1];
    if (type == 'index') {
        name = 'index.json';
    }
    if (name) {
        return `https://bucket-${bucket}-asset.kirafan.cn/${path.substring(0, path.length - 6)}/${name}`;
    } else if (type) {
        return `https://asset.kirafan.cn/${path}?type=${type}`;
    } else {
        return `https://bucket-${bucket}-asset.kirafan.cn/${path}`;
    }
}

export default { get, isDir, size, list, url };
