import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

import router from './router';
import './registerServiceWorker';
import '@/assetBundle';

window.vue = new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app');

String.prototype.splitAlphaAndDigit = function () {
  let ans = [];
  let index = 0;
  let digit = false;
  for (let i = 0; i < this.length; i++) {
    let digiti = this[i] >= '0' && this[i] <= '9';
    if (i == 0) {
      digit = digiti;
    }
    else if (digit != digiti) {
      ans.push(digit ? Number(this.substring(index, i)) : this.substring(index, i));
      digit = digiti;
      index = i;
    }
  }
  ans.push(this.substring(index));
  return ans;
};

String.sort = function (a, b) {
  a = a.splitAlphaAndDigit();
  b = b.splitAlphaAndDigit();
  for (let i = 0; ; i++) {
    if (a[i] === undefined && b[i] === undefined) break;
    if (a[i] > b[i]) return 1;
    if (a[i] < b[i]) return -1;
  }

  return 0;
};
