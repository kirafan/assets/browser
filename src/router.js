import Vue from 'vue';
import Router from 'vue-router';

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
};

Vue.use(Router);

export default new Router({
    base: process.env.BASE_URL,
    routes: [],
    scrollBehavior() {
        return { x: 0, y: 0 };
    }
});
